<!-- Refer to the single material legal review process (about.gitlab.com/handbook/legal/materials-legal-review-process/#track-1-single-material-legal-review-process) and complete this template to obtain legal review of a single piece of material. If you need to obtain legal review of multiple pieces of material with a related purpose, like several slide decks being prepared for one event, complete the multiple materials legal review issue template instead. -->

<!-- Title the issue as follows: “[name of material] Materials Legal Review”, for example “All Remote Evangelism Slide Deck Materials Legal Review”-->

### Materials to be reviewed
<!-- Link (for Google Docs) or upload (for other file types) the material for review here. --> 


### Is this material for internal or external use?
<!-- Delete as appropriate, and refer to the definitions of `external use` and `internal use` in the Materials Legal Review Process. If there are plans to use the material, or any part of it, externally in the future, chose `external`. -->
- external [@sfriss @LeeFalc]
- internal [@sfriss]

### Will the materials be made available on GitLab Unfiltered, Edcast, or anywhere else?
<!-- Delete as appropriate to state whether some or all of the materials being submitted for review will be made available anywhere. If they will, give details of the visibility the materials will have. -->
- yes <!-- if yes, give details -->
- no

### Is the material subject to mandatory review?
<!-- Delete as appropriate and refer to the definition of mandatory review: https://about.gitlab.com/handbook/legal/materials-legal-review-process/#mandatory-review -->
- yes <!-- if yes, identify the type of material from the list of materials subject to mandatory review -->
- no

### Does the material comply with the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/) and the [Guidelines for Use of Third-party IP in External Materials](https://about.gitlab.com/handbook/legal/ip-public-materials-guidelines/)?
<!-- Delete as appropriate -->
- yes
- no
- I'm not sure

### If the material is not subject to mandatory review and complies with the SAFE Framework and IP Guidelines, legal review is not mandatory. However, if you have specific questions regarding the material, set those out here, identifying the aspect(s) of the material to which the questions relate.
<!-- if the materials are subject to mandatory review, or do not comply with the SAFE Framework or IP Guidelines, skip to the next question -->

### Due date for review
<!-- State the due date for review, and indicate this as the due date of the issue below, noting that the Legal and Corporate Affairs Team requires at least two business days to complete a review. -->
----
<!-- Do Not Edit Below -->

### Note for materials creators
When creating materials:
- for internal use, follow the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/); and
- for external use, follow the [SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/) and the [Guidelines for Use of Third-party IP in External Materials](https://about.gitlab.com/handbook/legal/ip-public-materials-guidelines/).

Definitions of `external use` and `internal use` are set out in the [Materials Legal Review Process](https://about.gitlab.com/handbook/legal/materials-legal-review-process/#external-vs-internal-use).

/confidential
