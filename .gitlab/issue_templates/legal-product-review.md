### Legal Review Questionnaire

Legal review questionnaire related to: {ADD_URL}

### **Does the proposed solution involve any changes that will have an impact on:**

- the licensing of third-party software or dependencies;

  - [ ] Yes
  - [ ] No

- trademarks or patents;

  - [ ] Yes
  - [ ] No

- global trade compliance (or involve use of any new encryption algorithms or new key lengths);

  - [ ] Yes
  - [ ] No

- data privacy including capturing consent or changes to the collection or use of personal data of users or others; or

  - [ ] Yes
  - [ ] No

- anything else that should be raised with the Legal & Corporate Affairs team?

  - [ ] Yes
  - [ ] No

If 'No' to ALL of the above, indicate this by checking 'No' next to each question above. No further action is required at this time.

If 'yes' to ANY of the above, create a new comment on _this_ issue by copying and answering the questions to the related section below.

---

### **1. Licensing of third-party software or dependencies**

- 1. Does the proposed solution create a **new** dependency on a third-party project that is subject to a potentially acceptable (see bit.ly/3aEhwbo), unacceptable (see bit.ly/3uSLgbE), or proprietary license?

    -

### **2. Trademarks or patents**

- 1. Does the proposed solution:

        - a. Intend to be given a distinctive (rather than descriptive) name? See naming guidelines at bit.ly/3zbsjn2

            -

        - b. Involve the invention of a new and useful process that may be eligible for disclosure under the GitLab Patent Program (see bit.ly/3PirI8z)

            -

### **3. Global Trade Compliance**

- 1. Does the proposed solution involve the use of any new encryption algorithms or new key lengths?

    -

- 2. If so, do those algorithms (including the protocol and underlying crypto modules) leverage non-standard cryptography. Non-standard Cryptography is defined at bit.ly/3aE9kYH.

    -

- 3. Does the proposed solution:

        - a. Incorporate an open cryptographic interface? Open Cryptographic Interface is defined at bit.ly/3aMHY2B

            -

        - b. Involve (i) new pre-processing methods (e.g., data  compression or data interleaving) that are applied to plaintext data prior to encryption; or (ii) new post-processing methods (e.g., packetization, encapsulation) applied to the cipher text after encryption?

            -

        - c. Support any new communication protocols (e.g., X.25, Telnet, or TCP)?

            -

        - d. Involve the use of any compilers, runtime interpreters, or code assemblers, other than Ruby 2.4.4, Go 1.10, and Python 3?

            -

### **4. Data Privacy**

- 1. Does the proposed solution:

    - a. Involve the use of personal data? See definition in GitLab's Privacy Policy at about.gitlab.com/privacy

        -

    - b. Involve the collection of personal data not currently provided for in the GitLab Privacy Policy?

        -

    - c. Involve the use of personal data already permitted for collection under the GitLab Privacy Policy but for a new purpose that isn’t covered within the Policy?

        -

    - d. Permit the user to opt-out of the collection and use of their personal data? If not, explain why?.

        -

    - e. Involve the implementation of any new cookies, tracking pixels, or beacons? If yes, provide details.

        -

    - f. Involve any change to Service Ping (see bit.ly/3oaQ6Nm) or Snowplow (see bit.ly/3IFOcxL) data collection?

        -

    - g. Involve any change to the categorization of Services Usage Data (see bit.ly/3aJyNA3) as 'Subscription', 'Optional', or 'Operational'? If yes, provide details.

        -

    - h. Involve AI, machine learning, data matching from multiple sources, automated decision-making that could have a significant impact on individuals, or processing of biometric data like fingerprints or facial recognition? If yes, provide details.

        -

- 2. If personal data were not collected and used in the proposed solution, would GitLab function as normal albeit without the new functionality of the proposed solution?

    -

### **5. Anything else**

- 1. Does the proposed solution concern anything else that should be raised with the Legal & Corporate Affairs Team?

    -

/confidential 
/label ~"LRQ Submitted"
